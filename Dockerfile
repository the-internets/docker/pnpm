FROM node:18-alpine3.15

RUN apk --no-cache add curl git

RUN curl -f https://get.pnpm.io/v6.16.js | node - add --global pnpm

ENV NODE_OPTIONS --openssl-legacy-provider

